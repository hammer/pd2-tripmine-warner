-- Tripmine Warner (pd2-tripmine-warner)
-- Copyright (C) 2023  The Hammer (https://codeberg.org/hammer/)
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

dofile(ModPath .. "lib/MenuBuilder.lua")

TripMineWarner = TripMineWarner or {
  settings = {
    local_message_only            = false,
    send_global_message_as_client = false,
  }
}

menu_builder_inst = MenuBuilder:new("tripmine_warner", TripMineWarner.settings)
menu_builder_inst:load_settings()

Hooks:Add("MenuManagerBuildCustomMenus", "bltmod_tripmine_armed_warner_options_menu", function(menu_manager, nodes)
  menu_builder_inst:create_menu(nodes)
end)

Hooks:PostHook(UnitNetworkHandler, "sync_trip_mine_set_armed", "bltmod_tripmine_armed_warner", function(self, unit, new_state, length, sender)
  peer = self._verify_sender(sender) -- Returns peer depending on protocol
  if not peer -- Peer does not exist (probably redundant, checked in _verify_sender)
    or not managers.groupai:state():whisper_mode() -- Not in stealth
    or not new_state then -- Not set to explosive
    return
  end

  local warning_message = "[!!] " .. peer:name() .. " just armed a trip mine."

  if not TripMineWarner.settings.local_message_only
    and TripMineWarner.send_global_message_as_client 
    or not TripMineWarner.settings.send_global_message_as_client and LuaNetworking:IsHost() then
    managers.chat:send_message(ChatManager.GAME, managers.network.account:username(), warning_message)
  else
    managers.chat:feed_system_message(1, warning_message)
  end
end)
